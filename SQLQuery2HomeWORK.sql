/*
select * from COUNTRY;
SELECT * FROM CUSTOMERS ;
SELECT * FROM PRODUCTS;
SELECT * FROM ORDERS;
*/
--1
--select MAX(PRICE) from PRODUCTS;
--2
--SELECT p.NAME as name_of_most_expensive_product,o.quantity as purchased
--from PRODUCTS p
--inner join orders o on o.PRODUCT_ID =p.ID
--where p.PRICE =(select max(PRICE) from PRODUCTS)
--3
--SELECT * from orders o inner join customers c on o.customer_id = c.id;
--4
--SELECT * FROM  COUNTRY CO INNER JOIN CUSTOMERS C ON C.COUNTRY_CODE = CO.COUNTRY_CODE_ID
--ORDER BY CO.NAME;
--5
--select country.name, count(customers.country_code) as total_orders
--from customers 
--join orders on customers.id=orders.customer_id
--join country on customers.country_code = country.country_Code_Id
--group by country.name;
--6
--select co.NAME as country,sum(o.Quantity)as orders_quantity
--from customers c
--join orders o on c.id=o.customer_id
--join country co on c.country_code = co.country_Code_Id
--group by co.NAME;
--7
--select sum(p.price*o.Quantity) as sum_total_amount_of_all_orders from ORDERS o 
--join PRODUCTS p on p.ID=o.PRODUCT_ID;

