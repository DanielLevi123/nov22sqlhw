USE [Nov22HW]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 25/11/2020 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Country_CODE_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](50) NULL,
 CONSTRAINT [PK__Country__9E49137D58886B17] PRIMARY KEY CLUSTERED 
(
	[Country_CODE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CUSTOMERS]    Script Date: 25/11/2020 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUSTOMERS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](50) NOT NULL,
	[Country_CODE] [bigint] NOT NULL,
	[ADDRESS] [varchar](50) NULL,
	[Real_ID] [varchar](9) NOT NULL,
 CONSTRAINT [PK__CUSTOMER__3214EC27807F5E84] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ORDERS]    Script Date: 25/11/2020 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ORDERS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PRODUCT_ID] [bigint] NOT NULL,
	[CUSTOMER_ID] [bigint] NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK__ORDERS__3214EC2759379920] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PRODUCTS]    Script Date: 25/11/2020 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUCTS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](50) NOT NULL,
	[PRICE] [real] NOT NULL,
 CONSTRAINT [PK__PRODUCTS__3214EC2756DDD083] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Country] ON 

INSERT [dbo].[Country] ([Country_CODE_ID], [NAME]) VALUES (1, N'ISRAEL')
INSERT [dbo].[Country] ([Country_CODE_ID], [NAME]) VALUES (2, N'USA')
INSERT [dbo].[Country] ([Country_CODE_ID], [NAME]) VALUES (3, N'JAPAN')
INSERT [dbo].[Country] ([Country_CODE_ID], [NAME]) VALUES (4, N'FRANCE')
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
SET IDENTITY_INSERT [dbo].[CUSTOMERS] ON 

INSERT [dbo].[CUSTOMERS] ([ID], [NAME], [Country_CODE], [ADDRESS], [Real_ID]) VALUES (1, N'BILL GATES', 2, N'New York', N'033759232')
INSERT [dbo].[CUSTOMERS] ([ID], [NAME], [Country_CODE], [ADDRESS], [Real_ID]) VALUES (2, N'JOHN DOE', 4, N'PARIS', N'123456789')
INSERT [dbo].[CUSTOMERS] ([ID], [NAME], [Country_CODE], [ADDRESS], [Real_ID]) VALUES (3, N'BILL MORRIS', 2, N'California', N'033757422')
INSERT [dbo].[CUSTOMERS] ([ID], [NAME], [Country_CODE], [ADDRESS], [Real_ID]) VALUES (4, N'DANIEL HEN', 1, N'Tel Aviv', N'123546789')
SET IDENTITY_INSERT [dbo].[CUSTOMERS] OFF
GO
SET IDENTITY_INSERT [dbo].[ORDERS] ON 

INSERT [dbo].[ORDERS] ([ID], [PRODUCT_ID], [CUSTOMER_ID], [Quantity]) VALUES (1, 1, 1, 1)
INSERT [dbo].[ORDERS] ([ID], [PRODUCT_ID], [CUSTOMER_ID], [Quantity]) VALUES (2, 3, 2, 10)
INSERT [dbo].[ORDERS] ([ID], [PRODUCT_ID], [CUSTOMER_ID], [Quantity]) VALUES (3, 2, 2, 243)
INSERT [dbo].[ORDERS] ([ID], [PRODUCT_ID], [CUSTOMER_ID], [Quantity]) VALUES (4, 4, 3, 2)
SET IDENTITY_INSERT [dbo].[ORDERS] OFF
GO
SET IDENTITY_INSERT [dbo].[PRODUCTS] ON 

INSERT [dbo].[PRODUCTS] ([ID], [NAME], [PRICE]) VALUES (1, N'lAMBURGINI', 500000)
INSERT [dbo].[PRODUCTS] ([ID], [NAME], [PRICE]) VALUES (2, N'PENCIL', 0.5)
INSERT [dbo].[PRODUCTS] ([ID], [NAME], [PRICE]) VALUES (3, N'COMPUTER CHAIR', 1500)
INSERT [dbo].[PRODUCTS] ([ID], [NAME], [PRICE]) VALUES (4, N'Yacht', 5E+07)
INSERT [dbo].[PRODUCTS] ([ID], [NAME], [PRICE]) VALUES (5, N'Watermelon', 20.9)
SET IDENTITY_INSERT [dbo].[PRODUCTS] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__CUSTOMER__5FF042EAA5466EF1]    Script Date: 25/11/2020 13:58:20 ******/
ALTER TABLE [dbo].[CUSTOMERS] ADD  CONSTRAINT [UQ__CUSTOMER__5FF042EAA5466EF1] UNIQUE NONCLUSTERED 
(
	[Real_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CUSTOMERS]  WITH CHECK ADD  CONSTRAINT [FK__CUSTOMERS__Count__276EDEB3] FOREIGN KEY([Country_CODE])
REFERENCES [dbo].[Country] ([Country_CODE_ID])
GO
ALTER TABLE [dbo].[CUSTOMERS] CHECK CONSTRAINT [FK__CUSTOMERS__Count__276EDEB3]
GO
ALTER TABLE [dbo].[ORDERS]  WITH CHECK ADD  CONSTRAINT [FK_ORDERS_CUSTOMERS] FOREIGN KEY([CUSTOMER_ID])
REFERENCES [dbo].[CUSTOMERS] ([ID])
GO
ALTER TABLE [dbo].[ORDERS] CHECK CONSTRAINT [FK_ORDERS_CUSTOMERS]
GO
ALTER TABLE [dbo].[ORDERS]  WITH CHECK ADD  CONSTRAINT [FK_ORDERS_PRODUCTS] FOREIGN KEY([PRODUCT_ID])
REFERENCES [dbo].[PRODUCTS] ([ID])
GO
ALTER TABLE [dbo].[ORDERS] CHECK CONSTRAINT [FK_ORDERS_PRODUCTS]
GO
